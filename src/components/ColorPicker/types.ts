export type CheckerType = {
    minHue: number;
    maxHue: number;
    minSaturation: number;
    maxSaturation: number;
    minLightness: number;
    maxLightness: number;
    hue: number;
    saturation: number;
    lightness: number;
    rgb: Array<number>;
    cmyk: Array<number>;
    hex: string;
};

export type ColorObject = {
    rgb: {
        r: number;
        g: number;
        b: number;
    };
    hsl: {
        h: number;
        s: number;
        l: number;
    };
    cmyk: {
        c: number;
        m: number;
        y: number;
        k: number;
    };
    hex: string;
};
