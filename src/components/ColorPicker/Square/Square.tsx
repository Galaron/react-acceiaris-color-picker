import { useState, useRef, useEffect, MouseEventHandler } from 'react';
import styled from 'styled-components';
import throttle from 'lodash.throttle';
import { convertRGBtoHSL } from '../utils';
import Svg from '../Svg';
import usePaintSquare from './usePaintSquare';

type SquareProps = {
	hue: number;
	squareXY: Array<number>;
	setSaturation: (newValue: number) => void;
	setLightness: (newValue: number) => void;
	animate: boolean;
	changeAnimate: () => void;
	setSquareXY: (newValue: Array<number>) => void;
	squareSize: number;
	crossSize: number;
	saturationRange: Array<number>;
	lightnessRange: Array<number>;
	borderSize: number;
	innerShadow: string;
};

const Square = ({
	hue,
	squareXY,
	setSaturation,
	setLightness,
	animate,
	changeAnimate,
	setSquareXY,
	squareSize,
	crossSize,
	saturationRange,
	lightnessRange,
	borderSize,
	innerShadow,
}: SquareProps) => {
	const square = useRef<HTMLDivElement>(null);
	const canvas = useRef<HTMLCanvasElement>(null);

	// const [ctx, setCtx] = useState<CanvasRenderingContext2D | null>(null);
	const [isMouseDown, setIsMouseDown] = useState(false);

	usePaintSquare({
		canvas,
		hue,
		squareSize,
		saturationRange,
		lightnessRange,
	});

	useEffect(() => {
		if (!canvas || !canvas.current) {
			return;
		}

		const ctx = canvas.current.getContext('2d');
		if (!ctx) {
			return;
		}

		const [x, y] = squareXY;
		const x1 = Math.min(x + crossSize / 2, squareSize - 1);
		const y1 = Math.min(y + crossSize / 2, squareSize - 1);

		const [r, g, b] = Array.from(ctx.getImageData(x1, y1, 1, 1).data);
		const [, s, l] = convertRGBtoHSL([r, g, b]);
		setSaturation(s);
		setLightness(l);
	}, [squareXY]);

	const setPosition = (e: any) => {
		if (!isMouseDown) {
			return;
		}
		let newOffsetTop, newOffsetLeft;
		if (square && square.current) {
			newOffsetTop = square.current.offsetTop;
			newOffsetLeft = square.current.offsetLeft;
		} else {
			newOffsetTop = 0;
			newOffsetLeft = 0;
		}

		const scrolledTot = window.pageYOffset || (document.documentElement || document.body.parentNode || document.body).scrollTop;

		let x = Math.ceil(e.clientX - newOffsetLeft - crossSize / 2);
		let y = Math.ceil(e.clientY - newOffsetTop - crossSize / 2) + scrolledTot;

		if (x < -crossSize / 2) {
			x = -crossSize / 2;
		}
		if (x > squareSize - crossSize / 2) {
			x = squareSize - crossSize / 2;
		}

		if (y < -crossSize / 2) {
			y = -crossSize / 2;
		}
		if (y > squareSize - crossSize / 2) {
			y = squareSize - crossSize / 2;
		}
		setSquareXY([x, y]);
	};

	return (
		<SquareWrapper
			ref={square}
			squareSize={squareSize}
			borderSize={borderSize}
			innerShadow={innerShadow}
			onMouseDown={(e) => {
				setIsMouseDown(true);
				setPosition(e);
				changeAnimate();
			}}
			onMouseUp={(e) => {
				setIsMouseDown(false);
				setPosition(e);
				changeAnimate();
			}}
			onMouseMove={setPosition}
		>
			<Cross squareXY={squareXY} animate={animate} crossSize={crossSize}>
				<Svg name="cross" />
			</Cross>
			<Canvas ref={canvas} width={squareSize} height={squareSize} />
		</SquareWrapper>
	);
};

const SquareWrapper = styled.div<{
	squareSize: number;
	borderSize: number;
	innerShadow: string;
}>`
	position: relative;
	width: ${({ squareSize }) => squareSize}px;
	height: ${({ squareSize }) => squareSize}px;
	cursor: crosshair;
	box-shadow: 0 1px 2px ${({ innerShadow }) => innerShadow};
	background-color: green;
`;

const Canvas = styled.canvas.attrs((p) => ({}))``;

const Cross = styled.div<{
	squareXY: Array<number>;
	animate: boolean;
	crossSize: number;
}>`
	position: absolute;
	display: grid;
	justify-items: center;
	align-items: center;
	top: ${({ squareXY }) => squareXY[1]}px;
	left: ${({ squareXY }) => squareXY[0]}px;
	width: ${({ crossSize }) => crossSize}px;
	height: ${({ crossSize }) => crossSize}px;
	transition: ${({ animate }) => (animate ? 'top .25s ease-out, left .25s ease-out' : '0s')};
	svg {
		width: 100%;
		height: 100%;
	}
`;

export default Square;
