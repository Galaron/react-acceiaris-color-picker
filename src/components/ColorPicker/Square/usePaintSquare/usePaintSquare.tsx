import { useEffect } from "react";
import { convertHSLtoRGB } from "../../utils";

type PaintSquareProps = {
	canvas: any;
	hue: number;
	squareSize: number;
	saturationRange: Array<number>;
	lightnessRange: Array<number>;
};

const usePaintSquare = ({
	canvas,
	hue,
	squareSize,
	saturationRange,
	lightnessRange,
}: PaintSquareProps) => {
	useEffect(() => {
		if (!canvas.current) {
			return;
		}
		const ctx = canvas.current.getContext("2d");
		let colorLightness =
			lightnessRange[0] + (lightnessRange[1] - lightnessRange[0]) / 2;
		ctx.fillStyle = `hsl(${hue}, ${saturationRange[1]}%, ${colorLightness}%)`;
		ctx.fillRect(0, 0, squareSize, squareSize);
		const gradientWhite = ctx.createLinearGradient(0, 0, squareSize, 0);
		let whiteRGB;
		whiteRGB = convertHSLtoRGB([
			hue,
			saturationRange[0],
			lightnessRange[1],
		]);
		gradientWhite.addColorStop(
			0,
			`rgba(${whiteRGB[0]}, ${whiteRGB[1]}, ${whiteRGB[2]}, 1)`,
		);
		gradientWhite.addColorStop(
			1,
			`rgba(${whiteRGB[0]}, ${whiteRGB[1]}, ${whiteRGB[2]}, 0)`,
		);
		ctx.fillStyle = gradientWhite;
		ctx.fillRect(0, 0, squareSize, squareSize);
		const gradientBlack = ctx.createLinearGradient(0, 0, 0, squareSize);
		let blackRGB;
		blackRGB = convertHSLtoRGB([
			hue,
			saturationRange[1],
			lightnessRange[0],
		]);
		gradientBlack.addColorStop(
			0,
			`rgba(${blackRGB[0]}, ${blackRGB[1]}, ${blackRGB[2]}, 0)`,
		);
		gradientBlack.addColorStop(
			1,
			`rgba(${blackRGB[0]}, ${blackRGB[1]}, ${blackRGB[2]}, 1)`,
		);
		ctx.fillStyle = gradientBlack;
		ctx.fillRect(0, 0, squareSize, squareSize);
	}, [canvas, hue]);
};

export default usePaintSquare;
