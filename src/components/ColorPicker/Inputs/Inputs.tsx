import styled from 'styled-components';
import { HiSwitchVertical } from 'react-icons/hi';
import Input from './Input';
import { CheckerType } from '../types';

type InputsProps = {
	squareSize: number;
	models: Array<String>;
	labelColor: string;
	currentModel: number;
	hue: number;
	saturation: number;
	lightness: number;
	rgb: Array<number>;
	cmyk: Array<number>;
	hex: string;
	onHueChange: (newValue: number) => void;
	onSaturationChange: (newValue: number) => void;
	onLightnessChange: (newValue: number) => void;
	onHSLChange: (newValue: Array<number>) => void;
	switchModel: () => void;
	checker: CheckerType;
};

const Inputs = ({
	squareSize,
	models,
	currentModel,
	hue,
	saturation,
	lightness,
	rgb,
	cmyk,
	hex,
	onHueChange,
	onSaturationChange,
	onLightnessChange,
	onHSLChange,
	switchModel,
	checker,
	labelColor,
}: InputsProps) => {
	if (models[currentModel] === 'hsl') {
		return (
			<StyledInputs squareSize={squareSize}>
				<Input
					label="H"
					value={hue}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="S"
					value={saturation}
					changeHSL={onSaturationChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="L"
					value={lightness}
					changeHSL={onLightnessChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				{models.length > 1 ? (
					<StyledButton onClick={switchModel}>
						<HiSwitchVertical />
					</StyledButton>
				) : (
					<></>
				)}
			</StyledInputs>
		);
	}

	if (models[currentModel] === 'rgb') {
		return (
			<StyledInputs squareSize={squareSize}>
				<Input
					label="R"
					value={rgb[0]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="G"
					value={rgb[1]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="B"
					value={rgb[2]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				{models.length > 1 ? (
					<StyledButton onClick={switchModel}>
						<HiSwitchVertical />
					</StyledButton>
				) : (
					<></>
				)}
			</StyledInputs>
		);
	}

	if (models[currentModel] === 'cmyk') {
		return (
			<StyledInputs squareSize={squareSize}>
				<Input
					label="C"
					value={cmyk[0]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="M"
					value={cmyk[1]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="Y"
					value={cmyk[2]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				<Input
					label="K"
					value={cmyk[3]}
					changeHSL={onHueChange}
					changeColor={onHSLChange}
					squareSize={squareSize}
					checker={checker}
					labelColor={labelColor}
					switchModel={switchModel}
				/>
				{models.length > 1 ? (
					<StyledButton onClick={switchModel}>
						<HiSwitchVertical />
					</StyledButton>
				) : (
					<></>
				)}
			</StyledInputs>
		);
	}

	return (
		<StyledInputs squareSize={squareSize}>
			<Input
				label="HEX"
				value={hex}
				changeHSL={onHueChange}
				changeColor={onHSLChange}
				squareSize={squareSize}
				checker={checker}
				labelColor={labelColor}
				switchModel={switchModel}
			/>
			{models.length > 1 ? (
				<StyledButton onClick={switchModel}>
					<HiSwitchVertical />
				</StyledButton>
			) : (
				<></>
			)}
		</StyledInputs>
	);
};

const StyledInputs = styled.div<{ squareSize: number }>`
	width: ${({ squareSize }) => squareSize}px;
	display: flex;
	justify-content: space-evenly;
	align-items: center;
	justify-items: center;
	height: 20px;
`;

const StyledButton = styled.button`
	display: flex;
	align-items: center;
	justify-items: center;
`;

export default Inputs;
