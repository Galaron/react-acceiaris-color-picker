import { useState, useRef, useEffect } from "react";
import styled from "styled-components";
import { valueChecker } from "../../utils";
import { CheckerType } from "../../types";

type InputProps = {
	squareSize: number;
	labelColor: string;
	changeHSL: (newValue: number) => void;
	changeColor: (newValue: Array<number>) => void;
	switchModel: () => void;
	checker: CheckerType;
	label: string;
	value: number | string;
};

const Input = ({
	label,
	value,
	changeHSL,
	changeColor,
	squareSize,
	checker,
	labelColor,
}: InputProps) => {
	const [inputValue, setInputValue] = useState(value);
	const input = useRef(null);

	useEffect(() => {
		inputNewValue(value);
	}, [value]);

	const inputNewValue = (value: string | number) => {
		if (label === "HEX") {
			setInputValue(value);
		} else {
			if (typeof value === "number") {
				if (value >= 90) {
					setInputValue(Math.ceil(value));
				} else if (2 < value && value < 90) {
					setInputValue(Math.round(value));
				} else {
					setInputValue(Math.floor(value));
				}
			}
		}
	};

	const onChange = (e: any) => {
		setInputValue(e.target.value);
	};

	const onKeyPress = (e: any) => {
		if (e.key === "Enter") {
			update(e);
		}
	};

	const onBlur = (e: any) => {
		update(e);
	};

	const update = (e: any) => {
		let newValue;

		if (label === "HEX") {
			newValue = e.target.value;
			newValue = valueChecker(newValue, checker, label);
			changeColor(newValue[0]);
			inputNewValue(newValue[1]);
			return;
		}

		const n = parseFloat(e.target.value);
		if (label === "R" || label === "C") {
			newValue = valueChecker(n, checker, label);
			changeColor(newValue[0]);
			inputNewValue(newValue[1][0]);
			return;
		}
		if (label === "G" || label === "M") {
			newValue = valueChecker(n, checker, label);
			changeColor(newValue[0]);
			inputNewValue(newValue[1][1]);
			return;
		}
		if (label === "B" || label === "Y") {
			newValue = valueChecker(n, checker, label);
			changeColor(newValue[0]);
			inputNewValue(newValue[1][2]);
			return;
		}
		if (label === "K") {
			newValue = valueChecker(n, checker, label);
			changeColor(newValue[0]);
			inputNewValue(newValue[1][3]);
			return;
		}

		if (label === "H" || label === "S" || label === "L") {
			newValue = valueChecker(n, checker, label);
			changeHSL(newValue);
			inputNewValue(newValue);
			return;
		}
	};

	return (
		<InputWrapper
			squareSize={squareSize}
			label={label}
			labelColor={labelColor}
		>
			<label>{label}</label>
			<input
				ref={input}
				value={inputValue}
				onChange={onChange}
				onKeyPress={onKeyPress}
				onBlur={onBlur}
				autoFocus={false}
			/>
		</InputWrapper>
	);
};

const InputWrapper = styled.div<{
	squareSize: number;
	label: string;
	labelColor: string;
}>`
	display: flex;
	align-items: center;
	user-select: none;
	label {
		font-size: 11px;
		margin-right: 5px;
		color: ${({ labelColor }) => labelColor};
	}
	input {
		width: ${({ squareSize, label }) => {
			if (label === "HEX") {
				return squareSize / 1.5 + "px";
			}
			if (
				label === "C" ||
				label === "M" ||
				label === "Y" ||
				label === "K"
			) {
				return (squareSize / 250) * 25 + "px";
			}
			return (squareSize / 250) * 40 + "px";
		}};
		text-align: center;
		border: 1px solid #ddd;
		outline: 0;
		font-family: monospace;
		font-size: ${({ label }) => {
			if (label === "HEX") {
				return "12px";
			}
			if (
				label === "C" ||
				label === "M" ||
				label === "Y" ||
				label === "K"
			) {
				return "10px";
			}
			return "11px";
		}};
		padding: 4px 4px;
		user-select: none;
		&:focus {
			background: #fafafa;
		}
		&::selection {
			background: #ddd;
		}
	}
`;

export default Input;
