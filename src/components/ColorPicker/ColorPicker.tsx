import { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import Hue from './Hue';
import Square from './Square';
import Inputs from './Inputs';
import Modal from './Modal';
import { convertHSLtoRGB, convertRGBtoCMYK, convertRGBtoHEX, valueChecker } from './utils';
import { CheckerType, ColorObject } from './types';

export interface PickerProps {
	/** Function to call with the new ColorObject. ColorObject is in the form {rgb: {r, g, b}, hsl: {h, s, l}, cmyk: {c, m, y, k}, hex} */
	onColorChange: (newValue: ColorObject) => void;
	/** A React component  which on click will open the ColorPicker */
	parentNode?: React.ReactNode;
	/** Array of strings with the desired visible color model and the order they need to show up */
	models?: Array<String>;
	/** Dimension in pixel of the clickable square of colors */
	squareSize?: number;
	/** Height in pixel of the hue bar under the square. It can be only 10, 20, 30 or 40 */
	hueBarSize?: number;
	/** Range of hue colors as Array of two numbers, can accept ranges such as [300, 100], meaning the range from 300 to 360 plus the range from 0 to 100. */
	hueRange?: Array<number>;
	/** Range of lightness as Array of two numbers. */
	lightnessRange?: Array<number>;
	/** Number between 0 and 360. Value on the hue bar when the Color Picker gets rendered the first time. */
	startHue?: number;
	/** Number between 0 and 100. Saturation value when the Color Picker gets rendered the first time. */
	startSaturation?: number;
	/** Number between 0 and 100. Lightness value  when the Color Picker gets rendered the first time. */
	startLightness?: number;
	/** Color of the background of the Color Picker */
	backgroundColor?: string;
	/** Color of the labels of the color models (e.g. the letters "H", "S" and  "L") */
	labelColor?: string;
	/** Color of the shadow of the Color Picker */
	shadow?: string;
	/** Color of the shadow of the Color square */
	innerShadow?: string;
	/** Animation of the pointer when clicking on the square or on the hue bar */
	animation?: boolean;
	/** Size of the external border of the Color Picker */
	borderSize?: number;
	/** Shows a small circle left of the hue bar with the currently selected color */
	showColor?: boolean;
	/** Number of pixels of space between the square and the hue bar, and between the hue bar and the inputs */
	innerSpace?: number;
	/** A text on the top of the Color Picker */
	topPickerLabel?: string;
	/** A text on the bottom of the Color Picker */
	bottomPickerLabel?: string;
	/** Color of the text at the top and/or at the bottom of the Color Picker */
	pickerLabelColor?: string;
	/** Size in pixel of the text at the top and/or at the bottom of the Color Picker */
	pickerLabelSize?: number;
}

const ColorPicker = ({
	onColorChange,
	parentNode = null,
	models = ['hsl', 'rgb', 'cmyk', 'hex'],
	squareSize = 250,
	hueBarSize = 20,
	hueRange = [0, 360],
	lightnessRange = [0, 100],
	startHue = 0,
	startSaturation = 100,
	startLightness = 50,
	backgroundColor = 'white',
	labelColor = '#1a1a1a',
	shadow = 'rgba(0, 0, 0, 0.3)',
	innerShadow = 'rgba(0, 0, 0, 0.3)',
	animation = false,
	borderSize = 20,
	showColor = true,
	innerSpace = 10,
	topPickerLabel = '',
	bottomPickerLabel = '',
	pickerLabelColor = 'black',
	pickerLabelSize = 15,
}: PickerProps) => {
	const crossSize = 15;
	const saturationRange = [0, 100];
	let offset = 10;

	if (hueBarSize !== 10 && hueBarSize !== 20 && hueBarSize !== 30 && hueBarSize !== 40) {
		hueBarSize = 20;
	}
	if (hueBarSize === 10) {
		offset = 5;
	} else if (hueBarSize === 20) {
		offset = 10;
	} else if (hueBarSize === 30) {
		offset = 15;
	} else if (hueBarSize === 40) {
		offset = 20;
	}

	if (hueRange[0] < hueRange[1]) {
		if (!(hueRange[0] <= startHue && startHue <= hueRange[1])) {
			startHue = hueRange[0];
		}
	} else {
		if (hueRange[0] < startHue && startHue < hueRange[1]) {
			startHue = hueRange[1];
		}
	}

	if (startSaturation < 0 || startSaturation > 100) {
		startSaturation = 100;
	}

	if (!(lightnessRange[0] <= startLightness && startLightness <= lightnessRange[1])) {
		startLightness = lightnessRange[0];
	}

	if (lightnessRange[0] > lightnessRange[1]) {
		lightnessRange = [0, 100];
	}

	const computeHueX = (h: number) => {
		let hueXValue;
		let squareSizeReduced = squareSize;

		if (showColor) {
			squareSizeReduced -= 50;
		} else {
			squareSizeReduced -= 10;
		}

		if (hueRange[0] > hueRange[1]) {
			if (h < hueRange[0]) {
				h = h + 360;
			}
			hueXValue = ((h - hueRange[0]) / (360 - hueRange[0] + hueRange[1])) * squareSizeReduced - offset;
		} else {
			hueXValue = Math.round(((h - hueRange[0]) / (hueRange[1] - hueRange[0])) * squareSizeReduced - offset);
		}
		return hueXValue;
	};

	const computeSquareXY = (s: number, l: number) => {
		const t = (s * (l < 50 ? l : 100 - l)) / 100;
		const s1 = Math.round((200 * t) / (l + t)) | 0;
		const b1 = Math.round(t + l);
		const x = (squareSize / 100) * s1 - crossSize / 2;
		const y = squareSize - (squareSize / 100) * b1 - crossSize / 2;
		return [x, y];
	};

	const [hue, setHue] = useState(startHue);
	const [hueX, setHueX] = useState(computeHueX(startHue));
	const [saturation, setSaturation] = useState(startSaturation);
	const [lightness, setLightness] = useState(startLightness);
	const [squareXY, setSquareXY] = useState(computeSquareXY(startSaturation, startLightness));

	const [animate, setAnimate] = useState(animation);
	const [model, setModel] = useState(0);
	const [show, setShow] = useState(false);

	const [rgb, setRgb] = useState(convertHSLtoRGB([startHue, startSaturation, startLightness]));
	const [cmyk, setCmyk] = useState(convertHSLtoRGB([startHue, startSaturation, startLightness]));
	const [hex, setHex] = useState(convertRGBtoHEX(rgb));

	const [checker, setChecker] = useState<CheckerType>({
		minHue: hueRange[0],
		maxHue: hueRange[1],
		minSaturation: saturationRange[0],
		maxSaturation: saturationRange[1],
		minLightness: lightnessRange[0],
		maxLightness: lightnessRange[1],
		hue: hue,
		saturation: saturation,
		lightness: lightness,
		rgb: rgb,
		cmyk: cmyk,
		hex: hex,
	});

	const ref = useRef<HTMLDivElement>(null);

	useEffect(() => {
		setHueX(computeHueX(hue));
	}, []);

	useEffect(() => {
		const newRgb = convertHSLtoRGB([hue, saturation, lightness]);
		const newCmyk = convertRGBtoCMYK(newRgb);
		const newHex = convertRGBtoHEX(newRgb);
		setRgb(newRgb);
		setCmyk(newCmyk);
		setHex(newHex);
		onColorChange({
			rgb: {
				r: newRgb[0],
				g: newRgb[1],
				b: newRgb[2],
			},
			hsl: {
				h: hue,
				s: saturation,
				l: lightness,
			},
			cmyk: {
				c: newCmyk[0],
				m: newCmyk[1],
				y: newCmyk[2],
				k: newCmyk[3],
			},
			hex: newHex,
		});
		setChecker({
			minHue: hueRange[0],
			maxHue: hueRange[1],
			minSaturation: saturationRange[0],
			maxSaturation: saturationRange[1],
			minLightness: lightnessRange[0],
			maxLightness: lightnessRange[1],
			hue: hue,
			saturation: saturation,
			lightness: lightness,
			rgb: newRgb,
			cmyk: newCmyk,
			hex: newHex,
		});
	}, [hue, saturation, lightness]);

	const onHueChange = (n: number) => {
		const newHue = valueChecker(n, checker, 'H');
		setHue(newHue);
		setHueX(computeHueX(newHue));
	};

	const onSaturationChange = (n: number) => {
		const newSaturation = valueChecker(n, checker, 'S');
		setSaturation(newSaturation);
		setSquareXY(computeSquareXY(newSaturation, lightness));
	};

	const onLightnessChange = (n: number) => {
		const newLightness = valueChecker(n, checker, 'L');
		setLightness(newLightness);
		setSquareXY(computeSquareXY(saturation, newLightness));
	};

	const onHSLChange = (hsl: Array<number>) => {
		setHue(hsl[0]);
		setHueX(computeHueX(hsl[0]));
		setSaturation(hsl[1]);
		setLightness(hsl[2]);
		setSquareXY(computeSquareXY(hsl[1], hsl[2]));
	};

	const switchModel = () => {
		let newModel = (model + 1) % models.length;
		setModel(newModel);
	};

	const changeAnimate = () => {
		if (animation) {
			setAnimate(true);
		} else {
			setAnimate(false);
		}
	};

	const hueBar = () => {
		if (showColor) {
			return (
				<StyledHueBar squareSize={squareSize}>
					<StyledColor hue={hue} saturation={saturation} lightness={lightness} hueBarSize={hueBarSize} />
					<Hue
						hueX={hueX}
						animate={animate}
						setHue={setHue}
						setHueX={setHueX}
						squareSize={squareSize}
						hueBarSize={hueBarSize}
						hueRange={hueRange}
						showColor={showColor}
						innerSpace={innerSpace}
					/>
				</StyledHueBar>
			);
		}
		return (
			<Hue
				hueX={hueX}
				animate={animate}
				setHue={setHue}
				setHueX={setHueX}
				squareSize={squareSize}
				hueBarSize={hueBarSize}
				hueRange={hueRange}
				showColor={showColor}
				innerSpace={innerSpace}
			/>
		);
	};

	const pickerLabel = (label: string, position: string) => {
		if (label !== '') {
			if (position === 'top') {
				return (
					<StyledTopLabel
						pickerLabelColor={pickerLabelColor}
						pickerLabelSize={pickerLabelSize}
						innerSpace={innerSpace}
						borderSize={borderSize}
					>
						{label}
					</StyledTopLabel>
				);
			}
			return (
				<StyledBottomLabel
					pickerLabelColor={pickerLabelColor}
					pickerLabelSize={pickerLabelSize}
					innerSpace={innerSpace}
					borderSize={borderSize}
				>
					{label}
				</StyledBottomLabel>
			);
		}
	};

	const picker = (
		<PickerOuter squareSize={squareSize} borderSize={borderSize} backgroundColor={backgroundColor} shadow={shadow}>
			<PickerInner squareSize={squareSize} borderSize={borderSize} hueBarSize={hueBarSize}>
				{pickerLabel(topPickerLabel, 'top')}
				<Square
					hue={hue}
					squareXY={squareXY}
					animate={animate}
					changeAnimate={changeAnimate}
					setSaturation={setSaturation}
					setLightness={setLightness}
					setSquareXY={setSquareXY}
					squareSize={squareSize}
					crossSize={crossSize}
					saturationRange={saturationRange}
					lightnessRange={lightnessRange}
					borderSize={borderSize}
					innerShadow={innerShadow}
				/>
				{hueBar()}
				<Inputs
					squareSize={squareSize}
					models={models}
					currentModel={model}
					hue={hue}
					saturation={saturation}
					lightness={lightness}
					rgb={rgb}
					cmyk={cmyk}
					hex={hex}
					onHueChange={onHueChange}
					onSaturationChange={onSaturationChange}
					onLightnessChange={onLightnessChange}
					onHSLChange={onHSLChange}
					switchModel={switchModel}
					checker={checker}
					labelColor={labelColor}
				/>
				{pickerLabel(bottomPickerLabel, 'bottom')}
			</PickerInner>
		</PickerOuter>
	);

	return (
		<Wrapper ref={ref}>
			{parentNode === null ? (
				picker
			) : (
				<>
					<div onClick={() => setShow(true)}>{parentNode}</div>
					<Modal modal={ref} show={show} onClose={() => setShow(false)}>
						{picker}
					</Modal>
				</>
			)}
		</Wrapper>
	);
};

const Wrapper = styled.div`
	//
`;

const StyledTopLabel = styled.div<{
	pickerLabelColor: string;
	pickerLabelSize: number;
	innerSpace: number;
	borderSize: number;
}>`
	font-size: ${({ pickerLabelSize }) => pickerLabelSize}px;
	color: ${({ pickerLabelColor }) => pickerLabelColor};
	margin-top: ${({ innerSpace, borderSize }) => (borderSize < 10 ? innerSpace / 2 : 0)}px;
	margin-bottom: ${({ innerSpace, borderSize }) => (borderSize < 10 ? innerSpace / 2 : innerSpace)}px;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const StyledBottomLabel = styled.div<{
	pickerLabelColor: string;
	pickerLabelSize: number;
	innerSpace: number;
	borderSize: number;
}>`
	font-size: ${({ pickerLabelSize }) => pickerLabelSize}px;
	color: ${({ pickerLabelColor }) => pickerLabelColor};
	margin-bottom: ${({ innerSpace, borderSize }) => (borderSize < 10 ? innerSpace / 2 : 0)}px;
	margin-top: ${({ innerSpace, borderSize }) => (borderSize < 10 ? innerSpace / 2 : innerSpace)}px;
	display: flex;
	align-items: center;
	justify-content: center;
`;

const StyledHueBar = styled.div<{
	squareSize: number;
}>`
	width: ${({ squareSize }) => squareSize}px;
	display: flex;
	align-items: center;
	justify-content: space-evenly;
`;

const StyledColor = styled.div<{
	hue: number;
	saturation: number;
	lightness: number;
	hueBarSize: number;
}>`
	width: ${({ hueBarSize }) => hueBarSize / 1.5}px;
	height: ${({ hueBarSize }) => hueBarSize / 1.5}px;
	background-color: ${({ hue, saturation, lightness }) => `hsl(${hue}, ${saturation}%, ${lightness}%)`};
	border-radius: 100px;
`;

const PickerOuter = styled.div<{
	squareSize: number;
	borderSize: number;
	backgroundColor: string;
	shadow: string;
}>`
	width: ${({ squareSize, borderSize }) => squareSize + borderSize * 2}px;
	display: grid;
	border-radius: 2px;
	background: ${({ backgroundColor }) => backgroundColor};
	box-shadow: 0 3px 3px ${({ shadow }) => shadow};
`;

const PickerInner = styled.div<{
	squareSize: number;
	borderSize: number;
	hueBarSize: number;
}>`
	display: flex;
	flex-direction: column;
	margin-top: ${({ borderSize }) => borderSize}px;
	margin-bottom: ${({ borderSize }) => (borderSize > 5 ? borderSize : 5)}px;
	align-items: center;
	justify-items: center;
`;

export default ColorPicker;
