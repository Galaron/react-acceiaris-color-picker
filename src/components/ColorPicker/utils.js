export const convertRGBtoHSL = (rgb) => {
  const r = rgb[0] / 255;
  const g = rgb[1] / 255;
  const b = rgb[2] / 255;
  const min = Math.min(r, g, b);
  const max = Math.max(r, g, b);
  const delta = max - min;
  let h;
  let s;

  if (max === min) {
    h = 0;
  } else if (r === max) {
    h = (g - b) / delta;
  } else if (g === max) {
    h = 2 + (b - r) / delta;
  } else if (b === max) {
    h = 4 + (r - g) / delta;
  }

  h = Math.min(h * 60, 360);

  if (h < 0) {
    h += 360;
  }

  const l = (min + max) / 2;

  if (max === min) {
    s = 0;
  } else if (l <= 0.5) {
    s = delta / (max + min);
  } else {
    s = delta / (2 - max - min);
  }

  return [h, s * 100, l * 100];
};

export const convertHSLtoRGB = (
  hsl,
  hueRange = [0, 360],
  saturationRange = [0, 100],
  lightnessRange = [0, 100]
) => {
  let h = hsl[0];
  let s = hsl[1];
  let l = hsl[2];
  const quarterHueRange = (hueRange[1] - hueRange[0]) / 4;
  const quarterSaturationRange = (saturationRange[1] - saturationRange[0]) / 4;
  const quarterLightnessRange = (lightnessRange[1] - lightnessRange[0]) / 4;

  if (h >= hueRange[1] - quarterHueRange) {
    h = Math.ceil(h);
  } else if (
    hueRange[0] + quarterHueRange <
    h <
    hueRange[1] - quarterHueRange
  ) {
    h = Math.round(h);
  } else {
    h = Math.floor(h);
  }
  if (s >= saturationRange[1] - quarterSaturationRange) {
    s = Math.ceil(s);
  } else if (
    saturationRange[0] + quarterSaturationRange <
    s <
    saturationRange[1] - quarterSaturationRange
  ) {
    s = Math.round(s);
  } else {
    s = Math.floor(s);
  }
  if (l >= lightnessRange[1] - quarterLightnessRange) {
    l = Math.ceil(l);
  } else if (
    lightnessRange[0] + quarterLightnessRange <
    l <
    lightnessRange[1] - quarterLightnessRange
  ) {
    l = Math.round(l);
  } else {
    l = Math.floor(l);
  }

  s /= 100;
  l /= 100;

  const c = (1 - Math.abs(2 * l - 1)) * s;
  const x = c * (1 - Math.abs(((h / 60) % 2) - 1));
  const m = l - c / 2;
  let r = 0;
  let g = 0;
  let b = 0;

  if (0 <= h && h < 60) {
    r = c;
    g = x;
    b = 0;
  } else if (60 <= h && h < 120) {
    r = x;
    g = c;
    b = 0;
  } else if (120 <= h && h < 180) {
    r = 0;
    g = c;
    b = x;
  } else if (180 <= h && h < 240) {
    r = 0;
    g = x;
    b = c;
  } else if (240 <= h && h < 300) {
    r = x;
    g = 0;
    b = c;
  } else if (300 <= h && h <= 360) {
    r = c;
    g = 0;
    b = x;
  }
  r = (r + m) * 255;
  g = (g + m) * 255;
  b = (b + m) * 255;

  return [r, g, b];
};

export const convertRGBtoHEX = (rgb) => {
  return (
    "#" +
    colorToHex(Math.round(rgb[0])) +
    colorToHex(Math.round(rgb[1])) +
    colorToHex(Math.round(rgb[2]))
  );
};

const colorToHex = (c) => {
  let hex = c.toString(16);
  return hex.length === 1 ? "0" + hex : hex;
};

export const convertHEXtoRGB = (hex) => {
  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
  return [
    (parseInt(result[1], 16), parseInt(result[2], 16), parseInt(result[3], 16)),
  ];
};

export const convertRGBtoCMYK = (rgb) => {
  let k = (1 - Math.max(rgb[0], rgb[1], rgb[2]) / 255) * 100;
  let c = 0;
  let m = 0;
  let y = 0;
  if (k === 100) {
    c = m = y = 0;
  } else {
    c = ((100 - rgb[0] / 2.55 - k) / (100 - k)) * 100;
    m = ((100 - rgb[1] / 2.55 - k) / (100 - k)) * 100;
    y = ((100 - rgb[2] / 2.55 - k) / (100 - k)) * 100;
  }
  if (k < 0) {
    k = 0;
  }
  if (c < 0) {
    c = 0;
  }
  if (m < 0) {
    m = 0;
  }
  if (y < 0) {
    y = 0;
  }
  return [c, m, y, k];
};

export const convertCMYKtoRGB = (cmyk) => {
  const r = 255 * ((100 - cmyk[0]) / 100) * ((100 - cmyk[3]) / 100);
  const g = 255 * ((100 - cmyk[1]) / 100) * ((100 - cmyk[3]) / 100);
  const b = 255 * ((100 - cmyk[2]) / 100) * ((100 - cmyk[3]) / 100);
  return [r, g, b];
};

export const valueChecker = (value, checker, type, hueRange) => {
  if (type === "HEX") {
    let newHex;
    let newRgb = convertHEXtoRGB(value);
    const newHSL = convertRGBtoHSL(newRgb);
    const checkHSL = [
      valueChecker(newHSL[0], checker, "H"),
      valueChecker(newHSL[1], checker, "S"),
      valueChecker(newHSL[2], checker, "L"),
    ];
    newRgb = convertHSLtoRGB(checkHSL);
    newHex = convertRGBtoHEX(newRgb);
    return [checkHSL, newHex];
  }

  if (value < 0) {
    value = 0;
  }

  if (type === "H") {
    if (checker.minHue > checker.maxHue) {
      if (checker.maxHue < value && value < checker.minHue) {
        if (checker.minHue - value > value - checker.maxHue) {
          return checker.maxHue;
        }
        return checker.minHue;
      }
      return value;
    }
    if (value < checker.minHue) {
      return checker.minHue;
    }
    if (value > checker.maxHue) {
      return checker.maxHue;
    }
    return value;
  }
  if (type === "S") {
    if (value < checker.minSaturation) {
      return checker.minSaturation;
    }
    if (value > checker.maxSaturation) {
      return checker.maxSaturation;
    }
    return value;
  }
  if (type === "L") {
    if (value < checker.minLightness) {
      return checker.minLightness;
    }
    if (value > checker.maxLightness) {
      return checker.maxLightness;
    }
    return value;
  }

  if (type === "R" || type === "G" || type === "B") {
    if (value > 255) {
      value = 255;
    }

    let newRgb;
    if (type === "R") {
      newRgb = [value, checker.rgb[1], checker.rgb[2]];
    } else if (type === "G") {
      newRgb = [checker.rgb[0], value, checker.rgb[2]];
    } else {
      newRgb = [checker.rgb[0], checker.rgb[1], value];
    }
    let newHSL = convertRGBtoHSL(newRgb);
    const checkHSL = [
      valueChecker(newHSL[0], checker, "H"),
      valueChecker(newHSL[1], checker, "S"),
      valueChecker(newHSL[2], checker, "L"),
    ];
    newRgb = convertHSLtoRGB(checkHSL);
    return [checkHSL, newRgb];
  }

  if (type === "C" || type === "M" || type === "Y" || type === "K") {
    if (value > 100) {
      value = 100;
    }
    let newCmyk;
    if (type === "C") {
      newCmyk = [value, checker.cmyk[1], checker.cmyk[2], checker.cmyk[3]];
    } else if (type === "M") {
      newCmyk = [checker.cmyk[0], value, checker.cmyk[2], checker.cmyk[3]];
    } else if (type === "Y") {
      newCmyk = [checker.cmyk[0], checker.cmyk[1], value, checker.cmyk[3]];
    } else {
      newCmyk = [checker.cmyk[0], checker.cmyk[1], checker.cmyk[2], value];
    }
    let newRgb = convertCMYKtoRGB(newCmyk);
    const newHSL = convertRGBtoHSL(newRgb);
    const checkHSL = [
      valueChecker(newHSL[0], checker, "H"),
      valueChecker(newHSL[1], checker, "S"),
      valueChecker(newHSL[2], checker, "L"),
    ];
    newRgb = convertHSLtoRGB(checkHSL);
    newCmyk = convertRGBtoCMYK(newRgb);
    return [checkHSL, newCmyk];
  }
};
