import { useEffect } from "react";

type PaintHueProps = {
	squareSizeReduced: number;
	canvas: any;
	hueBarSize: number;
	hueRange: Array<number>;
};

const usePaintHue = ({
	canvas,
	squareSizeReduced,
	hueBarSize,
	hueRange,
}: PaintHueProps) => {
	useEffect(() => {
		const ctx = canvas.current.getContext("2d");
		ctx.rect(0, 0, squareSizeReduced, hueBarSize);

		const gradient = ctx.createLinearGradient(0, 0, squareSizeReduced, 0);
		let split = 0;
		if (hueRange[0] > hueRange[1]) {
			for (let i = hueRange[0]; i <= 360; i++) {
				gradient.addColorStop(split, `hsl(${i}, 100%, 50%)`);
				split += 1 / (362 - hueRange[0] + hueRange[1]);
			}
			for (let i = 0; i <= hueRange[1]; i++) {
				gradient.addColorStop(split, `hsl(${i}, 100%, 50%)`);
				split += 1 / (362 - hueRange[0] + hueRange[1]);
			}
		} else {
			for (let i = hueRange[0]; i <= hueRange[1]; i++) {
				gradient.addColorStop(split, `hsl(${i}, 100%, 50%)`);
				split += 1 / (hueRange[1] - hueRange[0] + 1);
			}
		}
		ctx.fillStyle = gradient;
		ctx.fill();
	}, [canvas]);
};

export default usePaintHue;
