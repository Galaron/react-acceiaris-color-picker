import React, { useRef, useEffect } from "react";
import styled from "styled-components";
import throttle from "lodash.throttle";
import Svg from "../Svg";
import usePaintHue from "./usePaintHue";

type HueProps = {
	hueX: number;
	setHue: (newValue: number) => void;
	setHueX: (newValue: number) => void;
	animate: boolean;
	squareSize: number;
	hueBarSize: number;
	hueRange: Array<number>;
	showColor: boolean;
	innerSpace: number;
};

const Hue = ({
	hueX,
	animate,
	setHue,
	setHueX,
	squareSize,
	hueBarSize,
	hueRange,
	showColor,
	innerSpace,
}: HueProps) => {
	const bar = useRef<HTMLDivElement>(null);
	const canvas = useRef<HTMLCanvasElement>(null);
	const offset = 10;
	let squareSizeReduced = squareSize;

	if (showColor) {
		squareSizeReduced -= 50;
	} else {
		squareSizeReduced -= 10;
	}

	usePaintHue({ canvas, squareSizeReduced, hueBarSize, hueRange });

	useEffect(() => {
		const computePosition = (e: any) => {
			let offsetLeft = 0;
			if (bar && bar.current) {
				offsetLeft = bar.current.offsetLeft;
			}
			return Math.max(
				hueBarSize / -2,
				Math.min(
					e.clientX - offsetLeft - hueBarSize / 2,
					squareSizeReduced - hueBarSize / 2,
				),
			);
		};

		const computeHue = (x: number) => {
			let hueValue = (x + offset) / squareSizeReduced;
			if (hueRange[0] > hueRange[1]) {
				hueValue = Math.round(
					(hueValue * (360 - hueRange[0] + hueRange[1]) +
						hueRange[0]) %
						360,
				);
			} else {
				hueValue = Math.round(
					hueValue * (hueRange[1] - hueRange[0]) + hueRange[0],
				);
			}
			let dimShift = 0;
			if (hueBarSize === 10) {
				dimShift = -9;
			} else if (hueBarSize === 20) {
				dimShift = 0;
			} else if (hueBarSize === 30) {
				dimShift = 9;
			} else if (hueBarSize === 40) {
				dimShift = 18;
			}
			return hueValue + dimShift;
		};

		const onMouseMove = throttle((e) => {
			const x = computePosition(e);
			let hue = computeHue(x);

			setHueX(x);
			setHue(hue);
		}, 0);

		const onMouseUp = (e: any) => {
			const x = computePosition(e);
			const hue = computeHue(x);
			setHueX(x);
			setHue(hue);
			document.body.removeEventListener("mousemove", onMouseMove);
			document.body.removeEventListener("mouseup", onMouseUp);
		};

		const onMouseDown = (e: any) => {
			document.body.addEventListener("mousemove", onMouseMove);
			document.body.addEventListener("mouseup", onMouseUp);
		};

		const barRef = bar.current;
		if (!barRef) {
			return;
		}
		barRef.addEventListener("mousedown", onMouseDown);

		return () => {
			barRef.removeEventListener("mousedown", onMouseDown);
			document.body.removeEventListener("mousemove", onMouseMove);
			document.body.removeEventListener("mouseup", onMouseUp);
		};
	}, [setHue, setHueX]);

	return (
		<HueWrapper
			ref={bar}
			squareSizeReduced={squareSizeReduced}
			hueBarSize={hueBarSize}
			innerSpace={innerSpace}
		>
			<Handle hueBarSize={hueBarSize} hueX={hueX} animate={animate}>
				<Svg name="handle" />
			</Handle>
			<Canvas
				ref={canvas}
				width={squareSizeReduced}
				height={hueBarSize}
			/>
		</HueWrapper>
	);
};

const HueWrapper = styled.div<{
	squareSizeReduced: number;
	hueBarSize: number;
	innerSpace: number;
}>`
	position: relative;
	width: ${({ squareSizeReduced }) => squareSizeReduced}px;
	height: ${({ hueBarSize }) => hueBarSize}px;
	cursor: ew-resize;
	margin-top: ${({ innerSpace }) => innerSpace}px;
	margin-bottom: ${({ innerSpace }) => innerSpace}px;
`;

const Canvas = styled.canvas`
	//
`;

const Handle = styled.div<{
	hueBarSize: number;
	hueX: number;
	animate: boolean;
}>`
	position: absolute;
	top: 0px;
	display: flex;
	align-items: center;
	justify-content: space-between;
	width: ${({ hueBarSize }) => hueBarSize}px;
	height: ${({ hueBarSize }) => hueBarSize}px;
	pointer-events: none;
	left: ${({ hueX }) => hueX}px;
	transition: ${({ animate }) => (animate ? "left .25s ease-out" : "0s")};
	svg {
		width: 100%;
		height: 100%;
	}
`;

export default Hue;
