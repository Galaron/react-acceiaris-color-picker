import React from "react";
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { ColorPicker } from "../components";

export default {
	title: "ColorPicker",
	component: ColorPicker,
};

export const Default = (args: any) => <ColorPicker {...args} />;

Default.args = {
	onColorChange: (color: any) => console.log(color),
	// startHue: 150,
	// startLightness: 30,
	// startSaturation: 75,
};
